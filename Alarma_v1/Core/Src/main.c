/* USER CODE BEGIN Header */

//Cableado:
/*
 * PANTALLA:///////// *
 * PA5 SCK (morado)
 * PA7 SDA (gris)
 * PC5 A0 (blanco)
 * PB1 RESET (marron)
 * PE7 CS (naranja)
 * GND gnd (negro)
 *
 * HX711:////////////
 * PE1 SCK (amarillo)
 * PE0 DT (verde)
 * 5V Vcc (rojo)
 * GND gnd (negro)
 *
 * POTENCIOMETRO://///
 * PA1 comun (gris)
 * 3.3V + (magenta)
 * GND - (negro)
 *
 * BUZZER:////////////
 * PD12 + (amarillo)
 * GND - (negro)
 *
 * SENSOR_VENTANA://///
 * PA2 Vs (marron)
 * 5V Vcc (rojo)
 * GND gnd (negro)
 * Divisor de tension resistencia de 10k a Vcc (Pull-up) se configura como pull-down porque es NC
 *
 * BOTÓN_USUARIO:///////
 * PC0 Vs (gris)
 * 5V Vcc (blanco)
 * GND gnd (morado)
 * Divisor de tension resistencia de 1500 a positivo (pull-up) con condensador (entre bornas de botón) de 10 000pF antirebotes 10ms
 *
 * RELÉS:////////
 * Vcc
 * GND
 * PC8 k1_LEDs (amarillo)
 * PA10 k2_electroiman (verde)
 *
 * LEDs///////////
 * GND gnd (negro)
 * PC7 Vcontrol (rojo)
 *
 *TODO: PROBLEMAS
 *El gettick deja de funcionar pasado un nº de tics -> se cambia de uint16_t a uint32
 *
 */


/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stdlib.h>

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

	//Librería de la pantalla
#include "ST7735.h"
#include "GFX_FUNCTIONS.h"

	//Libreria HX711
#include "hx711.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define deg_max 20
#define pres_max 100000
#define tiempo_espera 10

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

/* USER CODE BEGIN PV */
	//La galga extensiométrica:
HX711 peso;
int p=0;
int tara=0;
char presion[6];

	//Puerta
int deg=0;
char puerta[2];

	//banderas
volatile int boton =0;
volatile int ventana =0;

	//ESTADOS
bool Xstb, Xon, Xemer, Xfallo =0;

	//señal de alarma
int alarm=0;

	//Bit de trabajo
int bdt=0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM3_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM4_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


int  xfallo()
{
	//Imprimir por pantalla la info del fallo y el valor de los sensores

	  //Lectura de los sensores
	  itoa(deg, puerta, 10);

	  //Lectura del sensor de peso
	  p = HX711_Average_Value(peso, 4);
	  p = p - tara;
	  itoa(p, presion, 10);

	  //Pantalla
	  ST7735_SetRotation(1);
	  ST7735_WriteString(0, 0, "Fallo en los sensores", Font_7x10, WHITE,BLACK);

	  ST7735_WriteString(0, 10, "Puerta: ", Font_7x10, GREEN,BLACK);
	  if(deg>deg_max)
		  ST7735_WriteString(50, 10, puerta, Font_7x10, RED,BLACK);
	  else
		  ST7735_WriteString(50, 10, puerta, Font_7x10, WHITE,BLACK);


	  ST7735_WriteString(0, 20, "Presion: ", Font_7x10, GREEN,BLACK);
	  if(p<-100000)
		  ST7735_WriteString(50, 10, puerta, Font_7x10, RED,BLACK);
	  else
		  ST7735_WriteString(50, 20, presion, Font_7x10, WHITE,BLACK);

	  ST7735_WriteString(0, 30, "Ventana: ", Font_7x10, GREEN,BLACK);
	  if(ventana==1)
		  ST7735_WriteString(50, 30, "abierta", Font_7x10, RED,BLACK);
	  else
		  ST7735_WriteString(50, 30, "cerrada", Font_7x10, WHITE,BLACK);

	  ST7735_WriteString(0, 50, "Compruebe que los", Font_7x10, WHITE,BLACK);
	  ST7735_WriteString(0, 60, "sensores estan", Font_7x10, WHITE,BLACK);
	  ST7735_WriteString(0, 70, "por debajo del umbral", Font_7x10, WHITE,BLACK);
	  ST7735_WriteString(0, 80, "de activacion antes de", Font_7x10, WHITE,BLACK);
	  ST7735_WriteString(0, 90, "conectar la alarma.", Font_7x10, WHITE,BLACK);

	  //Electroiman
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);

	  //LEDs
	  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);

	  //Buzzer
	  htim4.Instance->CCR1 = 0;

	  return 0;
}

int xstb()
{
	uint32_t tickstart=0;

	//Pantalla
	ST7735_SetRotation(1);
	ST7735_WriteString(0, 0, "Alarma", Font_11x18, YELLOW,BLACK);
	ST7735_WriteString(0, 15, "desconectada", Font_11x18, YELLOW,BLACK);
	ST7735_WriteString(0, 50, "Pulse el boton azul", Font_7x10, WHITE,BLACK);
	ST7735_WriteString(0, 60, "para conectar.", Font_7x10, WHITE,BLACK);


	  //Lectura del sensor de peso
	  p = HX711_Average_Value(peso, 4);
	  p = p - tara;
	  if(p<-pres_max)
	  {
			//LEDs
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
			htim3.Instance->CCR2 = 20;
	  }
	  else HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);


	//electroiman
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);

	if(bdt==0)
	{
		HAL_NVIC_DisableIRQ(EXTI0_IRQn);
		//HAL_Delay(100);
		  tickstart = HAL_GetTick();
		  while((HAL_GetTick() - tickstart) < 100)
		  {
			  htim4.Instance->CCR1 = 10; 		//sonido de confirmacion
		  }

	  	  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
		  while((HAL_NVIC_GetPendingIRQ(EXTI0_IRQn)))
		  {
			  __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
			  HAL_NVIC_ClearPendingIRQ(EXTI0_IRQn);
		  }
		  boton=0;
		  ventana=0;

		  bdt=1;

	}


	//Buzzer
	htim4.Instance->CCR1 = 0;

	return 0;


}

int xon()
{
	uint32_t tickstart=0;


	//Pantalla
	ST7735_SetRotation(1);
	ST7735_WriteString(0, 0, "Alarma", Font_11x18, BLUE,BLACK);
	ST7735_WriteString(0, 15, "conectada", Font_11x18, BLUE,BLACK);
	ST7735_WriteString(0, 50, "Pulse el boton azul", Font_7x10, WHITE,BLACK);
	ST7735_WriteString(0, 60, "para desconectar.", Font_7x10, WHITE,BLACK);

	//electroiman
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);

	if(bdt==0)
	{
		HAL_NVIC_DisableIRQ(EXTI0_IRQn);
		//HAL_Delay(100);
		  tickstart = HAL_GetTick();
		  while((HAL_GetTick() - tickstart) < 100)
		  {
			  htim4.Instance->CCR1 = 10; 		//sonido de confirmacion
		  }

	  	  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
		  while((HAL_NVIC_GetPendingIRQ(EXTI0_IRQn)))
		  {
			  __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
			  HAL_NVIC_ClearPendingIRQ(EXTI0_IRQn);
		  }
		  boton=0;
		  ventana=0;

		  bdt=1;

	}

	//LEDs
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);

	//Buzzer
	htim4.Instance->CCR1 = 0;

	return 0;

}

int xemer()
{
	uint32_t tickstart=0;
	int t=0;
	char tiempo[2];

	//Causa de la alarma por pantalla
	switch (alarm)
	{
		case 0:
			//Pantalla
			ST7735_SetRotation(1);
			ST7735_WriteString(0, 0, "ERROR", Font_11x18, RED,BLACK);
			ST7735_WriteString(0, 15, "FATAL", Font_11x18, RED,BLACK);
			ST7735_WriteString(0, 50, "La alarma ha saltado sin motivo", Font_7x10, WHITE,BLACK);
			break;
		case 1:
			//Pantalla
			ST7735_WriteString(0, 90, "Causa: VENTANA", Font_7x10, WHITE,BLACK);
			break;
		case 2:
			//Pantalla
			ST7735_WriteString(0, 90, "Causa: PRESION", Font_7x10, WHITE,BLACK);
			break;
		case 3:
			//Pantalla
			ST7735_WriteString(0, 90, "Causa: PUERTA", Font_7x10, WHITE,BLACK);
			break;

	}

	if(bdt==0)
	{
		//Temporizador 10s para desactivar la alarma antes de que salten los avisos
		t=0;
		  tickstart = HAL_GetTick();
		  while(t <= tiempo_espera)
		  {
			  if(boton==1)
				  return 1;

			  t=(HAL_GetTick() - tickstart)/1000;
			  itoa(t, tiempo, 10);

				//Pantalla
				ST7735_SetRotation(1);
				ST7735_WriteString(0, 0, "Alarma", Font_11x18, RED,BLACK);
				ST7735_WriteString(0, 15, "ACTIVADA", Font_11x18, RED,BLACK);
				ST7735_WriteString(0, 50, "10s para desactivar", Font_7x10, WHITE,BLACK);
				ST7735_WriteString(80, 60, tiempo, Font_11x18, WHITE,BLACK);
				ST7735_FillRectangle(0, 100, t*16, 28, RED);
		  }
		  fillScreen(BLACK);

	}

	//Pantalla
	ST7735_SetRotation(1);
	ST7735_WriteString(0, 0, "Alarma", Font_11x18, RED,BLACK);
	ST7735_WriteString(0, 15, "ACTIVADA", Font_11x18, RED,BLACK);
	ST7735_WriteString(0, 50, "Pulse el boton azul   para desactivar", Font_7x10, WHITE,BLACK);

	//electroiman
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);

	if(bdt==0)
	{
		HAL_NVIC_DisableIRQ(EXTI0_IRQn);
		//HAL_Delay(100);
		  tickstart = HAL_GetTick();
		  while((HAL_GetTick() - tickstart) < 100)
		  {
			  //Tiempo de espera -> efectos parásitos
		  }

	  	  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
		  while((HAL_NVIC_GetPendingIRQ(EXTI0_IRQn)))
		  {
			  __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
			  HAL_NVIC_ClearPendingIRQ(EXTI0_IRQn);
		  }
		  boton=0;
		  ventana=0;

		  bdt=1;

	}

	//LEDs
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
	htim3.Instance->CCR2 = 50;


	//Buzzer
	tickstart=HAL_GetTick();
	while((HAL_GetTick()-tickstart)<200)
	{
		htim4.Instance->CCR1 = (HAL_GetTick()-tickstart);
	}

	return 0;


}


	//Bandera del botón de usuario
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){

	if (GPIO_Pin==GPIO_PIN_0)
	{
		boton=1;
	}

	if (GPIO_Pin==GPIO_PIN_2)
	{
		ventana=1;
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

		//Asignación del GPIO de la báscula
	peso.gpioSck = GPIOE;
	peso.gpioData = GPIOE;
	peso.pinSck = GPIO_PIN_1;
	peso.pinData = GPIO_PIN_0;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  MX_TIM3_Init();
  MX_ADC1_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */

  //uint16_t tickstart=0;

  	  //relés
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);

  	  //Inicialización de la pantalla
  ST7735_Init(0);
  fillScreen(BLACK);

  	  //Inicialización del PWM para el buzzer
  HAL_TIM_PWM_Start (&htim4, TIM_CHANNEL_1);
  	  //inicializacion del PWM para los LEDs
  HAL_TIM_PWM_Start (&htim3, TIM_CHANNEL_2);

	  //Interrupciones
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  	  //DMA potenciometro
  HAL_ADC_Start_DMA(&hadc1, &deg, 1);

  	  //Peso
  tara=HX711_Average_Value(peso, 20);

  	  //ESTADOS
  Xstb=1;
  Xon=0;
  Xemer=0;
  Xfallo=0;


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */


	  //ESTADO Xfallo
	  if(Xfallo==1)
	  {
		  //confirmación del estado por LEDs de la placa
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);

		  //Tratamiento
		  xfallo();


		  //SEGURIDAD
		  Xstb=0;
		  Xon=0;
		  Xemer=0;
	  }

	  //ESTADO Xsby
	  if(Xstb==1)
	  {
		  //confirmación del estado por LEDs de la placa
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);



		  //Tratamiento
		  xstb();


		  //Paso de Xstb->Xon
		  if(boton==1)
		  {
			  //resetear la pantalla para evitar fallos
			  fillScreen(BLACK);

			  //Lectura del sensor de peso
			  p = HX711_Average_Value(peso, 4);
			  p = p - tara;

			  //comprobar que la ventana esta cerrada
			  if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_2)!=1)
			  {
			  ventana=0;
			  }else ventana=1;


			  if(deg<deg_max && p>-pres_max && ventana==0)
			  {
				  Xstb=0;
				  Xon=1;
				  boton=0;
				  alarm=0;

		  		  bdt=0;

			  }else
			  {
				  Xstb=0;
				  Xfallo=1;
				  boton=0;
			  }

		  }


	  }


	  //ESTADO Xon
	  if(Xon==1)
	  {
		  //confirmación del estado por LEDs de la placa
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);

		  //tratamiento
		  xon();

		  //Paso de Xon->Xstb
		  if(boton==1)	//Hay que mantener el boton pulsado hasta el cambio de estado
		  {
			  //resetear la pantalla para evitar fallos
			  fillScreen(BLACK);

			  Xon=0;
			  Xstb=1;
			  boton=0;

			  bdt=0;
		  }

		  //Paso de Xon->Xemer


		  if(ventana==1)
			  alarm= 1;

		  //Lectura del sensor de peso
		  p = HX711_Average_Value(peso, 4);
		  p = p - tara;


		  if(p<=-pres_max)
			  alarm= 2;

		  if(deg>deg_max)
			  alarm= 3;

		  if(alarm!=0)
		  {
			  //resetear la pantalla para evitar fallos
			  fillScreen(BLACK);

			  //Comprobar que la ventana se ha cerrado antes de poner la ventana a 0
			  if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_2)!=1)
			  {
			  ventana=0;
			  }else ventana=1;


			  Xon=0;
			  Xemer=1;

			  bdt=0;
		  }
	  }

	  //Estado Xemer
	  if(Xemer==1)
	  {
		  //confirmación del estado por LEDs de la placa
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);

		  //Tratamiento
		  xemer();

		  //Paso de Xemer>->Xstb
		  if(boton==1)
		  {
			  //resetear la pantalla para evitar fallos
			  fillScreen(BLACK);

			  Xemer=0;
			  Xstb=1;
			  boton=0;

			  bdt=0;
		  }
	  }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_8B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 84-1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 100-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 336-1;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 100-1;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7|GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PA2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PC5 PC8 */
  GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PB1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PE7 PE1 */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PA10 */
  GPIO_InitStruct.Pin = GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PE0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
